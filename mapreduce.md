# Map Reduce

## Implementation Overview

* Map invovations are distributed to multiple machines by automaticlly partitioning the input data into a set of M split. The input split can be processed in parallel by different machine.

* Reduce invocationa are distributed by partitioning the intermediate key space into R pieces using a partitioning fucntion(hash(key) % R), the number of partions and partition function are specified by the user.

### Execution Overview

* The Map Reduce Lib first split the input data into M pieces of tipically 16 - 64M, then start many copy of the user program on a cluster of machines.

* One of the copy is special - the master. master assign pick idle workers and assigns each one a map task or a reduce task.

